/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-04 09:10:30
 * @LastEditTime: 2019-09-27 16:44:00
 * @LastEditors: Please set LastEditors
 */


// 正式环境
const domain="https://www.tongxikj.com/fuyou";//正式域名  
const domain_img = "https://www.tongxikj.com";  //正式拼接图片路径 

// 测试环境
// const domain = "https://www.tongxikj.com/fuyou/"; //测试域名
// const domain_img = "http://lele.tongxikj.com"  //测试拼接图片路径

const versionStr = "/api/v1";

// 请求API
const updateUser = domain + versionStr + '/user/updateUser'  //用户注册
const list = domain + versionStr + '/answer/list'  //题目列表
const uploadApi = domain + versionStr + '/file/uploadMathematicsFile'  //上传图片接口
const achievement = domain + versionStr + '/answer/answer' //提交成绩
var imgUrl = "https://www.tongxikj.com/img"
const equi = wx.getStorageSync('equi')

/**
 * @description: 拍照或者本地上传图片 
 * @param {obj} date 
 * // APPID = "5d6c7af2"
// API_KEY = "3e3344ea35f885b81ca1628bfe12a1ea"
// API_SECRET = "91861ba274aa0fbebc4251fe7ed78e1b"
// userId 用户id
// fileType 文件类型
// type 科目类型
 */
const upFiles = date => {
  wx.uploadFile({
    url: uploadApi,
    // filePath:filePathy,
    filePath: date.filePath,
    header: { 
      'Content-Type': 'application/x-www-form-urlencoded',
      "equi":equi
    },
    formData: {
      userId: wx.getStorageSync('userInfo').userId,
      fileType: 1,
      type: date.type
    },
    name: 'file',
    success: (resp) => {
      // console.log(resp)
      date.callback(resp.data)
    },
    fail: (res) => {
      // console.log(res)
    },
    complete: (res) => {
      // console.log(res)
    }
  });
}



const sent_http = function (url, params, callback, method = 'POST') {
  
  wx.request({
    url: url,
    data: params,
    method: method,
    header: {
      "Content-Type": "application/x-www-form-urlencoded",
      "devType": 1,
      "token": 123,
      "appId": "wx8aa72e1fe9cf62cc",
      "equi":equi
    },
    success(res) {
      // 这里的res有ret等信息
      res = res.data
      const ret = res.ret
      switch (ret) {
        case 1:
          callback(res.data)
          break;
        case 2:

          break;
        case 4:

          break;
        default:
          break;
      }
    },
    fail(res) {
      wx.showToast({
        title:'请检查网络~',
        duration:2000,
        icon:"none"
      })
    }
  })
}


export default {
  upFiles: upFiles,
  updateUser: function (data, callback) {
    sent_http(updateUser, data, callback)
  },
  question: function (data, callback) {
    sent_http(list, data, callback)
  },
  achievement:function (data, callback) {
    sent_http(achievement, data, callback)
  },
  imgUrl:imgUrl
}
